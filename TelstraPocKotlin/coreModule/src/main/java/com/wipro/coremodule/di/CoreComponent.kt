package com.wipro.coremodule.di

import android.content.Context
import android.content.SharedPreferences
import com.squareup.picasso.Picasso
import com.wipro.coremodule.networking.Scheduler
import dagger.Component
import retrofit2.Retrofit
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, NetworkModule::class, ImageModule::class])
interface CoreComponent {

    fun context(): Context

    fun retrofit(): Retrofit

    fun picasso(): Picasso

    fun scheduler(): Scheduler
}