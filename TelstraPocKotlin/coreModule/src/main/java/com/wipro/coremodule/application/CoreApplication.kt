package com.wipro.coremodule.application

import android.app.Application
import com.wipro.coremodule.di.AppModule
import com.wipro.coremodule.di.CoreComponent
import com.wipro.coremodule.di.DaggerCoreComponent

open class CoreApplication : Application() {

    companion object {
        lateinit var coreComponent: CoreComponent
    }

    override fun onCreate() {
        super.onCreate()
        initDI()

    }

    private fun initDI() {
        coreComponent = DaggerCoreComponent
            .builder()
            .appModule(AppModule(this))
            .build()
    }
}