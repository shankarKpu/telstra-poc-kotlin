package com.wipro.telstrapockotlin.ui.Feeds.model

import com.karntrehan.posts.core.testing.TestScheduler
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import com.wipro.coremodule.networking.Outcome
import com.wipro.telstrapockotlin.common.data.FeedBaseResponse
import com.wipro.telstrapockotlin.common.data.Row
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.TestObserver
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import java.io.IOException

/**
 * Tests for [FeedRespository]
 * */
@RunWith(JUnit4::class)
class FeedRespositoryTest {
    private val remote: FeedDataContract.Remote = mock()

    private lateinit var repository: FeedRepository
    private val compositeDisposable = CompositeDisposable()

    @Before
    fun init() {
        repository = FeedRepository(remote, TestScheduler(), compositeDisposable)
        whenever(remote.getFeedList()).doReturn(Single.just(FeedBaseResponse("")))
    }

    /**
     * Verify if calling [FeedRepository.fetchFeeds] triggers [FeedDataContract.Remote.getFeedList]
     *  and it's result is added to the [FeedRepository.feedFetchOutcome]
     * */
    @Test
    fun testFetchPosts() {

        val postWithUsersSuccess =
            Single.just(FeedBaseResponse("Title", listOf(Row("Title", "Description", ""))))
        whenever(remote.getFeedList()).doReturn(postWithUsersSuccess)

        val obs = TestObserver<Outcome<FeedBaseResponse>>()

        repository.feedFetchOutcome.subscribe(obs)
        obs.assertEmpty()

        repository.fetchFeeds()
        verify(remote).getFeedList()


        obs.assertValueAt(0, Outcome.loading(true))
        obs.assertValueAt(1, Outcome.loading(false))
        obs.assertValueAt(
            2,
            Outcome.success(FeedBaseResponse("Title", listOf(Row("Title", "Description", ""))))
        )
    }


    /**
     * Verify erred refresh of feed and pushes to [FeedDataContract.Repository.feedFetchOutcome]
     * with error
     * */
    @Test
    fun testRefreshPostsFailurePushesToOutcome() {
        val exception = IOException()
        whenever(remote.getFeedList()).doReturn(Single.error(exception))

        val obs = TestObserver<Outcome<FeedBaseResponse>>()
        repository.feedFetchOutcome.subscribe(obs)

        repository.refreshFeeds()

        obs.assertValueAt(0, Outcome.loading(true))
        obs.assertValueAt(1, Outcome.loading(false))
        obs.assertValueAt(2, Outcome.failure(exception))
    }
}