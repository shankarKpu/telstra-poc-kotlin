package com.wipro.telstrapockotlin.ui.Feeds.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import com.wipro.coremodule.networking.Outcome
import com.wipro.telstrapockotlin.common.data.FeedBaseResponse
import com.wipro.telstrapockotlin.common.data.Row
import com.wipro.telstrapockotlin.ui.Feeds.model.FeedDataContract
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import java.io.IOException

/**
 * Tests for [FeedListViewModel]
 */

@RunWith(JUnit4::class)
class FeedListViewModelTest {
    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var viewModel: FeedListViewModel

    private val repo: FeedDataContract.Repository = mock()

    private val outcome: Observer<Outcome<FeedBaseResponse>> = mock()

    @Before
    fun init() {
        viewModel = FeedListViewModel(repo, CompositeDisposable())
        whenever(repo.feedFetchOutcome).doReturn(PublishSubject.create())
        viewModel.feedFetchOutcome.observeForever(outcome)
    }

    /**
     * Test [FeedListViewModel.getFeeds] triggers [FeedDataContract.Repository.fetchFeeds] &
     * livedata [FeedListViewModel.feedFetchOutcome] gets outcomes pushed
     * from [FeedDataContract.Repository.feedFetchOutcome]
     * */
    @Test
    fun testGetPostsSuccess() {
        viewModel.getFeeds()
        verify(repo).fetchFeeds()

        repo.feedFetchOutcome.onNext(Outcome.loading(true))
        verify(outcome).onChanged(Outcome.loading(true))

        repo.feedFetchOutcome.onNext(Outcome.loading(false))
        verify(outcome).onChanged(Outcome.loading(false))

        val data = FeedBaseResponse("Title", listOf(Row("Title", "Description", "")))
        repo.feedFetchOutcome.onNext(Outcome.success(data))
        verify(outcome).onChanged(Outcome.success(data))
    }

    /**
     * Test that [ListDataContract.Repository.postFetchOutcome] on exception passes exception to
     * live [ListViewModel.postsOutcome]
     * */
    @Test
    fun testGetPostsError() {
        val exception = IOException()
        repo.feedFetchOutcome.onNext(Outcome.failure(exception))
        verify(outcome).onChanged(Outcome.failure(exception))
    }

    /**
     * Verify [ListViewModel.refreshPosts] triggers [ListDataContract.Repository.refreshPosts]
     * */
    @Test
    fun testRefreshPosts() {
        viewModel.refreshFeeds()
        verify(repo).refreshFeeds()
    }
}