package com.wipro.telstrapockotlin.common

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.wipro.coremodule.testing.DependencyProvider
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.*
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import java.io.IOException

@RunWith(JUnit4::class)
class FeedServiceTest {

    @Rule
    @JvmField
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var service:FeedService

    private lateinit var mockWebServer: MockWebServer

    @Before
    fun createService(){
        mockWebServer= MockWebServer()
        service = DependencyProvider
            .getRetrofit(mockWebServer.url("/"))
            .create(FeedService::class.java)
    }
    @After
    @Throws(IOException::class)
    fun stopService(){
        mockWebServer.shutdown()
    }

    @Test
    fun getPosts() {

        runBlocking {  queueResponse {
            setResponseCode(200)
            setBody(DependencyProvider.getResponseFromJson("feedResponse"))
        }


            service
                .getFeeds()
                .test()
                .run {
                    assertNoErrors()

                   // Assert.assertEquals(values()[0].size, 10)
                    Assert.assertEquals(values()[0].title, "About Canada")
                    assertValueCount(1)
                    Assert.assertEquals(values()[0].rows?.size, 14)
                    Assert.assertEquals(values()[0].rows?.get(0)?.title, "Beavers")
                    Assert.assertEquals(values()[0].rows?.get(0)?.description, "Beavers are second only to humans in their ability to manipulate and change their environment. They can measure up to 1.3 metres long. A group of beavers is called a colony")
                    Assert.assertEquals(values()[0].rows?.get(0)?.imageHref, "http://upload.wikimedia.org/wikipedia/commons/thumb/6/6b/American_Beaver.jpg/220px-American_Beaver.jpg")
                } }

    }
    private fun queueResponse(block: MockResponse.() -> Unit) {
        mockWebServer.enqueue(MockResponse().apply(block))
    }

}