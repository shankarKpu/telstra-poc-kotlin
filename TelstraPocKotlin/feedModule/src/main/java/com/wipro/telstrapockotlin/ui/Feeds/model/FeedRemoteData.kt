package com.wipro.telstrapockotlin.ui.Feeds.model

import com.wipro.telstrapockotlin.common.FeedService
import com.wipro.telstrapockotlin.common.data.FeedBaseResponse
import io.reactivex.Single

class FeedRemoteData(private val feedService: FeedService): FeedDataContract.Remote {
    override fun getFeedList()=feedService.getFeeds()
}