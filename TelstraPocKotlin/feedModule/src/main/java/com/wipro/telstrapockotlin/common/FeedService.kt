package com.wipro.telstrapockotlin.common

import com.wipro.coremodule.constants.Constants
import com.wipro.coremodule.constants.Constants.FEEDS
import com.wipro.telstrapockotlin.common.data.FeedBaseResponse
import io.reactivex.Single
import retrofit2.http.GET

interface FeedService {

    @GET(FEEDS)
    fun getFeeds(): Single<FeedBaseResponse>
}