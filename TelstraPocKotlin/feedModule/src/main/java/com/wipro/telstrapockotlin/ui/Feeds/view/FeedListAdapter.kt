package com.wipro.telstrapockotlin.ui.Feeds.view

import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.wipro.telstrapockotlin.R
import com.wipro.telstrapockotlin.common.data.Row
import kotlinx.android.synthetic.main.item_feed_view.view.*


class FeedListAdapter(private val picasso: Picasso) :
    ListAdapter<Row, FeedListAdapter.FeedListViewHolder>(FeedDataDC()) {

    var interaction: Interaction? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = FeedListViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_feed_view, parent, false), interaction
    )

    override fun onBindViewHolder(holder: FeedListViewHolder, position: Int) = holder.bind(getItem(position), picasso)

    fun setData(data: List<Row>) {
        submitList(data.toMutableList())
    }

    inner class FeedListViewHolder(itemView: View, private val interaction: Interaction?) :
        RecyclerView.ViewHolder(itemView), OnClickListener {

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val clicked = getItem(adapterPosition)
            interaction?.feedItemClick(
                clicked, itemView.title_txt,
                itemView.description_txt, itemView.feed_img
            )
        }

        fun bind(item: Row, picasso: Picasso) = with(itemView) {
            title_txt.text = item.title
            description_txt.text = item.description

            picasso.load(item.getAvatarPhoto())

                .resize(200, 150)
                .onlyScaleDown()
                .centerCrop()
                .error(R.drawable.no_image)
                .placeholder(R.drawable.loading_img)
                .into(itemView.feed_img)

            //SharedItem transition
            ViewCompat.setTransitionName(title_txt, item.title)
            ViewCompat.setTransitionName(description_txt, item.description)
            ViewCompat.setTransitionName(feed_img, item.getAvatarPhoto())
        }
    }

    interface Interaction {
        fun feedItemClick(
            feeds: Row,
            tvTitle: TextView,
            tvBody: TextView,
            ivAvatar: ImageView
        )
    }

    private class FeedDataDC : DiffUtil.ItemCallback<Row>() {
        override fun areItemsTheSame(oldItem: Row, newItem: Row) = oldItem == newItem

        override fun areContentsTheSame(oldItem: Row, newItem: Row) = oldItem == newItem
    }
}