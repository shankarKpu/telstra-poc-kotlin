package com.wipro.telstrapockotlin.ui.Feeds.model

import com.wipro.coremodule.extensions.*
import com.wipro.coremodule.networking.Outcome
import com.wipro.coremodule.networking.Scheduler
import com.wipro.telstrapockotlin.common.data.FeedBaseResponse
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject

class FeedRepository(
    private val remote: FeedDataContract.Remote,
    private val scheduler: Scheduler,
    private val compositeDisposable: CompositeDisposable
) : FeedDataContract.Repository {

    override val feedFetchOutcome: PublishSubject<Outcome<FeedBaseResponse>> =
        PublishSubject.create<Outcome<FeedBaseResponse>>()

    override fun fetchFeeds() {
        feedFetchOutcome.loading(true)
        remote.getFeedList()
            .performOnBackOutOnMain(scheduler)
            .subscribe({response -> feedFetchOutcome.success(response) },
                { error -> handleError(error)})
            .addTo(compositeDisposable)

    }

    override fun refreshFeeds() {
        fetchFeeds()
    }

    override fun handleError(error: Throwable) {
        feedFetchOutcome.failed(error)
    }


}