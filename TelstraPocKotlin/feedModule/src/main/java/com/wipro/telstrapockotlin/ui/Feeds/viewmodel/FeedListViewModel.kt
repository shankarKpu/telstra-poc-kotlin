package com.wipro.telstrapockotlin.ui.Feeds.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.wipro.coremodule.extensions.toLiveData
import com.wipro.coremodule.networking.Outcome
import com.wipro.telstrapockotlin.common.FeedModule
import com.wipro.telstrapockotlin.common.data.FeedBaseResponse
import com.wipro.telstrapockotlin.ui.Feeds.model.FeedDataContract
import io.reactivex.disposables.CompositeDisposable

class FeedListViewModel(private val repository: FeedDataContract.Repository,
                        private val compositeDisposable: CompositeDisposable): ViewModel()  {

    val feedFetchOutcome: LiveData<Outcome<FeedBaseResponse>> by lazy {
        repository.feedFetchOutcome.toLiveData(compositeDisposable)
    }

    fun getFeeds(){
        if(feedFetchOutcome.value==null)
            repository.fetchFeeds()
    }
    fun refreshFeeds(){
        repository.refreshFeeds()
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
        FeedModule.destroyListComponent()

    }
}