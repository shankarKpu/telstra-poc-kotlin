package com.wipro.telstrapockotlin.ui.Feeds.view

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.wipro.coremodule.base.BaseActivity
import com.wipro.coremodule.extensions.isConnected
import com.wipro.coremodule.networking.Outcome
import com.wipro.telstrapockotlin.R
import com.wipro.telstrapockotlin.common.FeedModule
import com.wipro.telstrapockotlin.common.data.FeedBaseResponse
import com.wipro.telstrapockotlin.common.data.Row
import com.wipro.telstrapockotlin.ui.Feeds.viewmodel.FeedListViewModel
import com.wipro.telstrapockotlin.ui.Feeds.viewmodel.FeedListViewModelFactory
import kotlinx.android.synthetic.main.feed_list.*
import java.io.IOException
import javax.inject.Inject

class FeedListActivity : BaseActivity()
    , FeedListAdapter.Interaction {
    private val component by lazy { FeedModule.listComponent() }
    @Inject
    lateinit var viewModelFactory: FeedListViewModelFactory

    @Inject
    lateinit var adapter: FeedListAdapter

    private val viewModel: FeedListViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory)
            .get(FeedListViewModel::class.java)
    }

    private val context: Context by lazy { this }

    private val TAG = "FeedListActivity"


    fun intiBinding() {
        adapter.interaction = this
        feedsRecyclerView.adapter = adapter
        swipeToRefresh.setOnRefreshListener { viewModel.refreshFeeds() }
        viewModel.getFeeds()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.feed_list)
        component.inject(this)
        intiBinding()
        if (isConnected) {
            initiateDataListener()
        } else {
            Toast.makeText(
                context,
                R.string.network_status,
                Toast.LENGTH_SHORT
            ).show()
        }

    }

    private fun initiateDataListener() {
        //Observe the outcome and update state of the screen  accordingly
        viewModel.feedFetchOutcome.observe(this,
            Observer<Outcome<FeedBaseResponse>> { outcome ->
                Log.d(TAG, "initiateDataListener: $outcome")
                when (outcome) {

                    is Outcome.Progress -> {
                        swipeToRefresh.isRefreshing = outcome.loading

                    }
                    is Outcome.Success -> {


                        val filterData = outcome.data.rows?.filterNot { it -> it.title == null }

                        Log.d(TAG, "initiateDataListener: Successfully loaded data")
                        filterData?.let {

                            // Set toolbar title/app title
                            supportActionBar!!.title = outcome.data.title

                            adapter.setData(it)
                        }
                    }

                    is Outcome.Failure -> {
                        swipeToRefresh.isRefreshing = false
                        if (outcome.e is IOException)
                            Toast.makeText(
                                context,
                                outcome.e.message,
                                Toast.LENGTH_SHORT
                            ).show()
                        else
                            Toast.makeText(
                                context,
                                R.string.failure,
                                Toast.LENGTH_SHORT
                            ).show()
                    }


                }
            })
    }

    override fun feedItemClick(
        feeds: Row,
        tvTitle: TextView,
        tvBody: TextView,
        ivAvatar: ImageView
    ) {


    }
}




