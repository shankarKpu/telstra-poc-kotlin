package com.wipro.telstrapockotlin.ui.Feeds.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.wipro.telstrapockotlin.ui.Feeds.model.FeedDataContract
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.internal.disposables.ArrayCompositeDisposable

class FeedListViewModelFactory(private val repository: FeedDataContract.Repository,
                               private val compositeDisposable: CompositeDisposable): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return FeedListViewModel(repository,compositeDisposable) as T
    }
}