package com.wipro.telstrapockotlin.common

import com.wipro.coremodule.application.CoreApplication
import com.wipro.telstrapockotlin.di.DaggerFeedComponent
import com.wipro.telstrapockotlin.di.FeedComponent
import javax.inject.Singleton

@Singleton
object FeedModule {

    private var feedComponent: FeedComponent? = null

    fun listComponent(): FeedComponent {
        if (feedComponent == null)
            feedComponent = DaggerFeedComponent.builder()
                .coreComponent(CoreApplication.coreComponent).build()
        return feedComponent as FeedComponent

    }

    fun destroyListComponent() {
        feedComponent = null
    }
}