package com.wipro.telstrapockotlin.ui.Feeds.model

import com.wipro.coremodule.networking.Outcome
import com.wipro.telstrapockotlin.common.data.FeedBaseResponse
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject

interface FeedDataContract {

    interface Repository{

       val feedFetchOutcome: PublishSubject<Outcome<FeedBaseResponse>>
        fun fetchFeeds()
        fun refreshFeeds()
        fun handleError(error: Throwable)

    }
    interface Remote {
        fun getFeedList() : Single<FeedBaseResponse>
    }
}