package com.wipro.telstrapockotlin.di

import com.squareup.picasso.Picasso
import com.wipro.coremodule.di.CoreComponent
import com.wipro.coremodule.networking.Scheduler
import com.wipro.telstrapockotlin.common.FeedService
import com.wipro.telstrapockotlin.ui.Feeds.model.FeedDataContract
import com.wipro.telstrapockotlin.ui.Feeds.model.FeedRemoteData
import com.wipro.telstrapockotlin.ui.Feeds.model.FeedRepository
import com.wipro.telstrapockotlin.ui.Feeds.view.FeedListActivity
import com.wipro.telstrapockotlin.ui.Feeds.view.FeedListAdapter
import com.wipro.telstrapockotlin.ui.Feeds.viewmodel.FeedListViewModelFactory
import dagger.Component
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit

@FeedScope
@Component(dependencies = [CoreComponent::class], modules = [FeedListModule::class])
interface FeedComponent {

    fun feedService(): FeedService
    fun picasso(): Picasso
    fun scheduler(): Scheduler

    fun inject(listActivity: FeedListActivity)
}

@Module
@FeedScope
class FeedListModule {

    /*Adapter*/
    @Provides
    @FeedScope
    fun adapter(picasso: Picasso): FeedListAdapter = FeedListAdapter(picasso)

    /*ViewModel*/
    @Provides
    @FeedScope
    fun feedListViewModelFactory(repository: FeedDataContract.Repository,
                             compositeDisposable: CompositeDisposable):
            FeedListViewModelFactory = FeedListViewModelFactory(repository,
        compositeDisposable)

    /*Repository*/
    @Provides
    @FeedScope
    fun feedRepo(remote: FeedDataContract.Remote,
                 scheduler: Scheduler,
                 compositeDisposable: CompositeDisposable):
            FeedDataContract.Repository =
        FeedRepository(remote, scheduler, compositeDisposable)

    @Provides
    @FeedScope
    fun remoteData(postService: FeedService):
            FeedDataContract.Remote = FeedRemoteData(postService)


    @Provides
    @FeedScope
    fun compositeDisposable(): CompositeDisposable = CompositeDisposable()


    @Provides
    @FeedScope
    fun feedService(retrofit: Retrofit): FeedService = retrofit.create(FeedService::class.java)
}