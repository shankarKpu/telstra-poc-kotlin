package com.wipro.telstrapockotlin.common.data

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.util.ArrayList

@Parcelize
data class FeedBaseResponse(
    @SerializedName("title") var title: String?,
    @SerializedName("rows") var rows: List<Row>? = ArrayList()
) : Parcelable {
}