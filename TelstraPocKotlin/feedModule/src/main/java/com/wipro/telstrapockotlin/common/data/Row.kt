package com.wipro.telstrapockotlin.common.data

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Row(
    @SerializedName("title") var title: String?,
    @SerializedName("description") var description: String?,
    @SerializedName("imageHref") var imageHref: String?
) : Parcelable {

    fun getAvatarPhoto() = imageHref
}